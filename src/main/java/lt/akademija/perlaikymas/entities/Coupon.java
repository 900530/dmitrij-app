package lt.akademija.perlaikymas.entities;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Coupon {
	@Id
	@GeneratedValue
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date date;
	@NotNull
	@NotBlank
	private String seller;
	@Size(min=1)
	private String buyer;
	
	@OneToMany(mappedBy="coupon", orphanRemoval=true, fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	private List<Item> items;
	
	public Coupon(){}
	
	public Coupon(Date date, String buyer, String seller) {
		super();
		this.date = date;
		this.buyer = buyer;
		this.seller = seller;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSeller() {
		return buyer;
	}

	public void setSeller(String buyer) {
		this.buyer = buyer;
	}

	public String getBuyer() {
		return seller;
	}

	public void setBuyer(String seller) {
		this.seller = seller;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String convertDateToString(Date date){
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String s = formatter.format(date);
		return s;
	}
}
