package lt.akademija.perlaikymas.models;

import lt.akademija.perlaikymas.entities.Item;

public class ItemModel {
	
	private Item currentItem;

	public Item getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(Item currentItem) {
		this.currentItem = currentItem;
	}
}
