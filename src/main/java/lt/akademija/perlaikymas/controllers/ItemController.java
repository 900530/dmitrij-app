package lt.akademija.perlaikymas.controllers;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lt.akademija.perlaikymas.entities.Coupon;
import lt.akademija.perlaikymas.entities.Item;
import lt.akademija.perlaikymas.models.ItemModel;

public class ItemController {

	private static final Logger logger = LogManager.getLogger(ItemController.class.getName());
	
	private CouponController couponController;
	private ItemModel itemModel;
	
	
	public String create(){
		itemModel.setCurrentItem(new Item());
		return "update-item";
	}
	
	public String update(Item item){
		logger.info("Item updated");
		itemModel.setCurrentItem(item);
		return "update-item";
	}
	
	public String delete(Item item){
		Coupon coupon = couponController.getCouponModel().getCurrentCoupon();
		Long id = coupon.getId();
		coupon.getItems().remove(item);
		couponController.save();
		couponController.getCouponModel().setCurrentCoupon(couponController.findCouponById(id));
		logger.info("Item was deleted");
		return "update-coupon";
		
	}
	
	public String save(){
		Coupon coupon = couponController.getCouponModel().getCurrentCoupon();
		Item item = itemModel.getCurrentItem();
		Long id = coupon.getId();
		if (!coupon.getItems().contains(item)){
			item.setCoupon(coupon);
			coupon.getItems().add(item);
		}
		couponController.save();
		couponController.getCouponModel().setCurrentCoupon(couponController.findCouponById(id));
		if(itemModel.getCurrentItem().getId() == null){
			logger.info("New item created");
		}
		else logger.info("Item was updated");
		return "update-coupon";
	}
	
	public String cancel(){
		itemModel.setCurrentItem(null);
		Coupon coupon = couponController.getCouponModel().getCurrentCoupon();
		Long id = coupon.getId();
		couponController.getCouponModel().setCurrentCoupon(couponController.findCouponById(id));
		return "update-coupon";
	}

	public CouponController getCouponController() {
		return couponController;
	}

	public void setCouponController(CouponController couponController) {
		this.couponController = couponController;
	}

	public ItemModel getItemModel() {
		return itemModel;
	}

	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}

}
