package lt.akademija.perlaikymas.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Item {
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	@NotBlank
	private String name;
	@Min(0)
	private Integer amount;
	private String unit;
	@NotNull
	private Integer price;
	
//	@JoinColumn(name="coupon_id")
	@ManyToOne(optional = true)
	private Coupon coupon;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Coupon getCoupon() {
		return coupon;
	}
	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}
	public Integer getTotal(){
		if (amount == null || price == null){
			return 0;
		}
		return (Integer)(amount*price);
	}
	
}
