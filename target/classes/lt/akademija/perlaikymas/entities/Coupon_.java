package lt.akademija.perlaikymas.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Coupon.class)
public abstract class Coupon_ {

	public static volatile SingularAttribute<Coupon, Date> date;
	public static volatile SingularAttribute<Coupon, String> seller;
	public static volatile SingularAttribute<Coupon, Long> id;
	public static volatile ListAttribute<Coupon, Item> items;
	public static volatile SingularAttribute<Coupon, String> buyer;

}

