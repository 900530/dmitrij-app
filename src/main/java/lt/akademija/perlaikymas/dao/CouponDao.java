package lt.akademija.perlaikymas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import lt.akademija.perlaikymas.entities.Coupon;
import lt.akademija.perlaikymas.entities.Item;

public class CouponDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public List<Coupon> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Coupon> cq = cb.createQuery(Coupon.class);
			Root<Coupon> root = cq.from(Coupon.class);
			cq.select(root); // we select entity here
			TypedQuery<Coupon> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public List<Item> findAllItems() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Item> cq = cb.createQuery(Item.class);
			Root<Item> root = cq.from(Item.class);
			cq.select(root); // we select entity here
			TypedQuery<Item> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public void save(Coupon newCoupon) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(newCoupon))
			newCoupon = em.merge(newCoupon);
		em.persist(newCoupon);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Coupon invoice) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		invoice = em.merge(invoice);
		em.remove(invoice);
		em.getTransaction().commit();
		em.close();
	}

	public Coupon findById(Long id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Coupon> authorQuery = em.createQuery("SELECT i From Coupon i WHERE i.id = :id", Coupon.class);
		authorQuery.setParameter("id", id);
		authorQuery.setMaxResults(1);
		return authorQuery.getSingleResult();
	}

	public Integer findMaxPrice() {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Integer> authorQuery = em.createQuery("SELECT max(i.price) From Item i", Integer.class);
		return authorQuery.getSingleResult();
	}

	public void deleteItem(Item item) {
		em = entityManagerFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			item = em.merge(item);
			em.remove(item);
			Coupon invoice = item.getCoupon();
			invoice = em.merge(invoice);
			em.persist(invoice);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}
}
