package lt.akademija.perlaikymas.models;

import javax.validation.Valid;

import lt.akademija.perlaikymas.entities.Coupon;
import lt.akademija.perlaikymas.entities.Item;

public class CouponModel {
	@Valid
	private Coupon currentCoupon;
	private Double total;

	public Coupon getCurrentCoupon() {
		return currentCoupon;
	}

	public void setCurrentCoupon(Coupon currentCoupon) {
		this.currentCoupon = currentCoupon;
	}
	
	public Double getCalculatedSum(){
		Double sum = 0.0;
		for (Item item: getCurrentCoupon().getItems()){
			sum = sum + item.getAmount()*item.getPrice();
		}
		return sum;
	}

	public Double getTotal() {
		return total;
	}
}
