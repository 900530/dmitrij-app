package lt.akademija.perlaikymas.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lt.akademija.perlaikymas.dao.CouponDao;
import lt.akademija.perlaikymas.entities.Coupon;
import lt.akademija.perlaikymas.entities.Item;
import lt.akademija.perlaikymas.models.CouponModel;

public class CouponController {
	
	private static final Logger logger = LogManager.getLogger(CouponController.class.getName());
	
	private CouponModel couponModel;
	private CouponDao couponDao;
	

	public List<Coupon> getCouponList() {
		return couponDao.findAll();
	}
	
	public String create(){
		couponModel.setCurrentCoupon(new Coupon());
		return "create-coupon";
	}
	
	public String update(Coupon coupon){
		couponModel.setCurrentCoupon(coupon);
		logger.info("Coupon updated");
		return "update-coupon";
	}
	
	public String cancel(){
		couponModel.setCurrentCoupon(null);
		return "coupon-list";
	}
	
	public String save(){
		couponDao.save(couponModel.getCurrentCoupon());
		if(couponModel.getCurrentCoupon().getId() == null){
			logger.info("New coupon created");
		}
		else logger.info("Coupon updated");
		return "coupon-list";
	}
	
	public Coupon findCouponById(Long id){
		return couponDao.findById(id);
	}
	
	public void delete(Coupon coupon){
		logger.info("Coupon was deleted");
		couponDao.delete(coupon);
	}

	public String overview(Coupon coupon){
		couponModel.setCurrentCoupon(coupon);
		return "coupon-overview";
	}
	
	public CouponModel getCouponModel() {
		return couponModel;
	}

	public void setCouponModel(CouponModel couponModel) {
		this.couponModel = couponModel;
	}

	public CouponDao getCouponDao() {
		return couponDao;
	}

	public void setCouponDao(CouponDao couponDao) {
		this.couponDao = couponDao;
	}

	public List<Item> getAllItems(){
		return couponDao.findAllItems();
	}
}
