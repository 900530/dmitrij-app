package lt.akademija.perlaikymas.controllers;

import org.primefaces.context.RequestContext;

public class BackingBean {
	CouponController couponController;
	ItemController itemController;
	private boolean priceTooHigh;
	
	public boolean getShowNotificationBar(){
		if (couponController.getCouponList().size()<2){
			return true;
		}
		return false;
	}
	
	public boolean getShowNotificationBarForItems(){
		if (couponController.getCouponModel().getCurrentCoupon().getItems().size() < 1){
			return true;
		}
		return false;
	}
	
	public void changeColorOnPrice(){
		RequestContext rc = RequestContext.getCurrentInstance();
		Integer totalPrice = itemController.getItemModel().getCurrentItem().getTotal();
		if ( totalPrice < 2){
			rc.execute("document.getElementById('forma:out').setAttribute('class', 'redcolor')");
			priceTooHigh = false;
		}
		if (totalPrice > 2){
			rc.execute("document.getElementById('forma:out').setAttribute('class', 'greencolor')");
			priceTooHigh = false;
		}
		if (totalPrice >1000){
			priceTooHigh = true;
			rc.update("forma:mess");
		}
		
	}
			
	public CouponController getCouponController() {
		return couponController;
	}
	public void setCouponController(CouponController couponController) {
		this.couponController = couponController;
	}
	public ItemController getItemController() {
		return itemController;
	}
	public void setItemController(ItemController itemController) {
		this.itemController = itemController;
	}

	public boolean isPriceTooHigh() {
		return priceTooHigh;
	}
	
}
