package lt.akademija.perlaikymas.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Item.class)
public abstract class Item_ {

	public static volatile SingularAttribute<Item, Integer> amount;
	public static volatile SingularAttribute<Item, String> unit;
	public static volatile SingularAttribute<Item, Coupon> coupon;
	public static volatile SingularAttribute<Item, Integer> price;
	public static volatile SingularAttribute<Item, String> name;
	public static volatile SingularAttribute<Item, Long> id;

}

